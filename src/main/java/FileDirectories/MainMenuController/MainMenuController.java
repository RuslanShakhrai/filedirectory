package FileDirectories.MainMenuController;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javax.swing.*;

public class MainMenuController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button AddNewFileButton;

    @FXML
    private Button ShowFileDirectoryButton;

    @FXML
    private Button ExitButton;

    @FXML
    private Button AboutButton;

    @FXML
    private Button GetFilesWithMaxRequestsButton;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MainMenuController mainMenuController = new MainMenuController();
        ExitButton.setOnAction(actionEvent -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Вы действительно хотите выйти?", "Подтверждение действия", JOptionPane.YES_NO_OPTION);
            if (input == JOptionPane.YES_OPTION) {
                System.exit(0);

            }
        });
        AboutButton.setOnAction(actionEvent -> {
            JOptionPane.showMessageDialog(null, "Данная программа разрабатывается в учебных целях.", "О программе",JOptionPane.INFORMATION_MESSAGE);
        });
        AddNewFileButton.setOnAction(actionEvent -> {
            String path = "/view/AddNewFile.fxml";
            String title = "Добавление нового файла в каталог";
            mainMenuController.moveForm(AddNewFileButton,path,title);

        });
        ShowFileDirectoryButton.setOnAction(actionEvent -> {
            String path = "/view/ShowInfoAboutDirectory.fxml";
            String title = "Просмотр каталога файлов";
            mainMenuController.moveForm(ShowFileDirectoryButton,path,title);
        });
        GetFilesWithMaxRequestsButton.setOnAction(actionEvent -> {
            String path = "/view/GetFilesWithMaxCounter.fxml";
            String title = "Файлы с максимальным количеством обращений";
            mainMenuController.moveForm(GetFilesWithMaxRequestsButton,path,title);
        });
    }
    public void moveForm(Button button,String path,String title)
    {
        button.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(path));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        Image ico = null;
        try {
            ico = new Image(new FileInputStream("src/main/resources/images/icon.png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        stage.getIcons().add(ico);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.show();
    }

    public void moveMainMenu(Button button)
    {
        String path="/view/MainMenu.fxml";
        String title="Файловый каталог";
        moveForm(button,path,title);
    }

}
