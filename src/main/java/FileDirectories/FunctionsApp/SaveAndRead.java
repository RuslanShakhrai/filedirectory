package FileDirectories.FunctionsApp;

import FileDirectories.Main;
import FileDirectories.MainMenuController.MainMenuController;
import FileDirectories.model.MyFile;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

public class SaveAndRead {
    protected LinkedList<MyFile> directory = new LinkedList<>();
    public void saveFilesIntoDirectory(MyFile myFile)
    {
        String path = "Directory.json";
        SaveAndRead saveAndRead = new SaveAndRead();
        File file = new File(path);
        if(file.length()== 0 )
        {
            directory.add(myFile);
            saveAndRead.saveInfo(path,directory);
        }
        else{
            directory = getDirectoryData();
            directory.add(myFile);
            saveAndRead.saveInfo(path,directory);
        }
    }
    public  void saveInfo(String path, LinkedList linkedList) {
        File file = new File(path);
        ObjectMapper objectMapper = new ObjectMapper();
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
            writer.writeValue(fileOutputStream, linkedList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static LinkedList<MyFile> getDirectoryData()
    {
        String filePath="Directory.json";
        LinkedList<MyFile>linkedList=new LinkedList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            File file = new File(filePath);
            linkedList = mapper.readValue(file, new TypeReference<LinkedList<MyFile>>() {});
        }
        catch (MismatchedInputException ex)
        {
            JOptionPane.showMessageDialog(null,"Данные отсутствуют ","Error",JOptionPane.ERROR_MESSAGE);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return linkedList;
    }

}
