package FileDirectories.Controller;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

import FileDirectories.FunctionsApp.SaveAndRead;
import FileDirectories.MainMenuController.MainMenuController;
import FileDirectories.model.MyFile;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import javax.swing.*;

public class AddNewFileController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private DatePicker DateOfFilePicker;

    @FXML
    private TextField TitleOfFileInputField;

    @FXML
    private TextField CounterOfCallsField;

    @FXML
    private Button ConfirmButton;

    @FXML
    private Button BackButton;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MainMenuController mainMenuController = new MainMenuController();
        ConfirmButton.setOnAction(event -> {
                    int input = JOptionPane.showConfirmDialog(null, "Вы готовы подтвердить данные?", "Подтверждение данных", JOptionPane.YES_NO_OPTION);
                    if(input==JOptionPane.YES_OPTION)
                    {
                        try{
                            String title = TitleOfFileInputField.getText();
                            TextField editableDate = DateOfFilePicker.getEditor();
                            String date = editableDate.getText();
                            Date dateOfFile = null;
                            dateOfFile = new SimpleDateFormat("dd.MM.yyyy").parse(date);
                            if(DateOfFilePicker.getValue()!=null)
                            {
                                LocalDate localDate = DateOfFilePicker.getValue();
                                dateOfFile = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                                simpleDateFormat.format(dateOfFile);
                            }
                            String inputCounter = CounterOfCallsField.getText();
                            int counterOfCalls = Integer.parseInt(inputCounter);
                            MyFile myFile = new MyFile(title, dateOfFile,counterOfCalls);
                            JOptionPane.showMessageDialog(null,myFile,"Добавленный файл",JOptionPane.INFORMATION_MESSAGE);
                            SaveAndRead saveAndRead = new SaveAndRead();
                            saveAndRead.saveFilesIntoDirectory(myFile);
                            mainMenuController.moveMainMenu(ConfirmButton);
                        }
                        catch (NumberFormatException ex){
                            JOptionPane.showMessageDialog(null,"Количество вызовов должно быть числом","Error",JOptionPane.ERROR_MESSAGE);
                        }
                        catch (ParseException ex)
                        {
                            JOptionPane.showMessageDialog(null,"Введите дату в указанном формате","Warning",JOptionPane.WARNING_MESSAGE);
                        }
                    }
                });

            BackButton.setOnAction(actionEvent -> {
            int confirmExit= JOptionPane.showConfirmDialog(null,"Вы действительно хотите вернуться в главное меню? ","Сделайте выбор",JOptionPane.YES_NO_OPTION);
            if(confirmExit==JOptionPane.YES_OPTION)
            {
                mainMenuController.moveMainMenu(BackButton);
            }

        });
    }
}
