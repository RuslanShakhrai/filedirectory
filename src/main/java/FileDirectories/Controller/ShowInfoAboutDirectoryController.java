package FileDirectories.Controller;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;

import FileDirectories.FunctionsApp.SaveAndRead;
import FileDirectories.MainMenuController.MainMenuController;
import FileDirectories.model.MyFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.swing.*;

public class ShowInfoAboutDirectoryController extends SaveAndRead implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<MyFile> tableDirectory;

    @FXML
    private TableColumn<MyFile,String> TitleOfFileColumn;

    @FXML
    private TableColumn<MyFile,Date> DateOfFileColumn;

    @FXML
    private TableColumn<MyFile,Integer> CounterOfCallsColumn;

    @FXML
    private Button BackButton;

    @FXML
    private DatePicker DateConditionPicker;

    @FXML
    private Button DeleteFilesButton;

    @FXML
    private Button ReferToFileButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MainMenuController mainMenuController = new MainMenuController();
        TitleOfFileColumn.setCellValueFactory(new PropertyValueFactory<MyFile,String>("title"));
        DateOfFileColumn.setCellFactory(column -> {
            TableCell<MyFile, Date> cell = new TableCell<MyFile, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };
            return cell;
        });
        DateOfFileColumn.setCellValueFactory(new PropertyValueFactory<MyFile,Date>("date"));
        CounterOfCallsColumn.setCellValueFactory(new PropertyValueFactory<MyFile,Integer>("counter"));
            directory = getDirectoryData();
            ObservableList<MyFile> directoryData = FXCollections.observableArrayList();
            directoryData.addAll(directory);
            tableDirectory.setItems(directoryData);
        DeleteFilesButton.setOnAction(actionEvent -> {
            Date comparebleDate = null;
            try {
                TextField editableDate = DateConditionPicker.getEditor();
                String date = editableDate.getText();
                comparebleDate = new SimpleDateFormat("dd.MM.yyyy").parse(date);
                if (DateConditionPicker.getValue() != null) {
                    LocalDate localDate = DateConditionPicker.getValue();
                    comparebleDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    simpleDateFormat.format(comparebleDate);
                }
            }catch (ParseException exception)
            {
                JOptionPane.showMessageDialog(null,"Введите дату в указанном формате","Warning",JOptionPane.WARNING_MESSAGE);
            }
            int input= JOptionPane.showConfirmDialog(null,"Вы действительно хотите удалить все файлы из каталога, которые были созданы ранее указанной даты? ","Подтверждение действия",JOptionPane.YES_NO_OPTION);
            if(input==JOptionPane.YES_OPTION) {
                Iterator<MyFile> myFileIterator = directory.iterator();
                while (myFileIterator.hasNext()) {
                    MyFile nextFile = myFileIterator.next();
                    if (nextFile.getDate().before(comparebleDate)) {
                        myFileIterator.remove();
                    }
                }
                ShowInfoAboutDirectoryController showInfoAboutDirectoryController = new ShowInfoAboutDirectoryController();
                showInfoAboutDirectoryController.saveInfo("Directory.json", directory);
                JOptionPane.showMessageDialog(null, "Все файлы, созданные позднее указанной даты были усешно удалены", "Success", JOptionPane.INFORMATION_MESSAGE);
                mainMenuController.moveMainMenu(DeleteFilesButton);
            }
        });
        ReferToFileButton.setOnAction(actionEvent -> {
            MyFile myFile = tableDirectory.getSelectionModel().getSelectedItem();
            tableDirectory.setItems(directoryData);
            int selectedIndex = directory.indexOf(myFile);
            if (selectedIndex == -1) {
                JOptionPane.showMessageDialog(null, "Вы ничего не выбрали", "Warning", JOptionPane.WARNING_MESSAGE);
            }
            else {
                int input = JOptionPane.showConfirmDialog(null,
                        "Вы действительно хотите обратиться к выбранному вами файлу?", "Подтверждение действия", JOptionPane.YES_NO_OPTION);
                if (input == JOptionPane.YES_OPTION) {
                    MyFile selectedFile = directory.get(selectedIndex);
                    selectedFile.setCounter(selectedFile.getCounter()+1);
                    ShowInfoAboutDirectoryController showInfoAboutDirectoryController = new ShowInfoAboutDirectoryController();
                    showInfoAboutDirectoryController.saveInfo("Directory.json", directory);
                    JOptionPane.showMessageDialog(null,"Вы успешно обратились к файлу","Success",JOptionPane.INFORMATION_MESSAGE);
                    tableDirectory.refresh();
                }
            }
        });

        BackButton.setOnAction(actionEvent -> {
            int confirmExit= JOptionPane.showConfirmDialog(null,"Вы действительно хотите вернуться в главное меню? ","Сделайте выбор",JOptionPane.YES_NO_OPTION);
            if(confirmExit==JOptionPane.YES_OPTION)
            {
                mainMenuController.moveMainMenu(BackButton);
            }
        });
    }
}
