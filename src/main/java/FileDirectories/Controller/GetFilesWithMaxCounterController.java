package FileDirectories.Controller;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;

import FileDirectories.FunctionsApp.SaveAndRead;
import FileDirectories.MainMenuController.MainMenuController;
import FileDirectories.model.MyFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.swing.*;


public class GetFilesWithMaxCounterController extends SaveAndRead implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<MyFile> tableDirectory;

    @FXML
    private TableColumn<MyFile, String> TitleOfFileColumn;

    @FXML
    private TableColumn<MyFile, Date> DateOfFileColumn;

    @FXML
    private TableColumn<MyFile, Integer> CounterOfCallsColumn;

    @FXML
    private Button BackButton;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MainMenuController mainMenuController = new MainMenuController();
        TitleOfFileColumn.setCellValueFactory(new PropertyValueFactory<MyFile, String>("title"));
        DateOfFileColumn.setCellFactory(column -> {
            TableCell<MyFile, Date> cell = new TableCell<MyFile, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });
        DateOfFileColumn.setCellValueFactory(new PropertyValueFactory<MyFile, Date>("date"));
        CounterOfCallsColumn.setCellValueFactory(new PropertyValueFactory<MyFile, Integer>("counter"));
        directory = getDirectoryData();
        try {
            int max = directory.get(0).getCounter();
            LinkedList<MyFile> maxCounterData = new LinkedList<>();
            Iterator<MyFile> myFileIterator = directory.iterator();
            while (myFileIterator.hasNext()) {
                MyFile nextFile = myFileIterator.next();
                if (nextFile.getCounter() > max) {
                    max = nextFile.getCounter();
                    maxCounterData.add(nextFile);
                    maxCounterData.remove(0);
                } else if (nextFile.getCounter() == max) {
                    maxCounterData.add(nextFile);
                }
            }
            ObservableList<MyFile> maxCounterFilesData = FXCollections.observableArrayList();
            maxCounterFilesData.addAll(maxCounterData);
            tableDirectory.setItems(maxCounterFilesData);
        }
        catch (IndexOutOfBoundsException ex)
        {
            JOptionPane.showMessageDialog(null,"Каталог пуст!!! ","Error",JOptionPane.ERROR_MESSAGE);
        }
        BackButton.setOnAction(actionEvent -> {
            int confirmExit = JOptionPane.showConfirmDialog(null, "Вы действительно хотите вернуться в главное меню? ", "Сделайте выбор", JOptionPane.YES_NO_OPTION);
            if (confirmExit == JOptionPane.YES_OPTION) {
                mainMenuController.moveMainMenu(BackButton);
            }

        });
    }
}
