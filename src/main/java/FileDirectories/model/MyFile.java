package FileDirectories.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyFile implements Serializable {
    private String title;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd.MM.yyyy")
    private Date date;
    private int counter;//количество обращений к файлу
    public MyFile(String title, Date date, int counter)
    {
        this.title=title;
        this.date=date;
        this.counter=counter;
    }
    public MyFile(){}

    public String getTitle()
    {
        return title;
    }
    public Date getDate()
    {
        return date;
    }
    public int getCounter()
    {
        return counter;
    }
    public void setTitle(String title)
    {
        this.title=title;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String formatedDate;
        formatedDate = simpleDateFormat.format(date);
        return
                "Название: " + title + '\n' +
                "дата создания файла: " + formatedDate +'\n'+
                "количество обращениий к файлу: " + counter;
    }
}
