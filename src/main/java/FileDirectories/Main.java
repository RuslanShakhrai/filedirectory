package FileDirectories;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import java.io.FileInputStream;

/**
 * JavaFX App
 */
public class Main extends Application {
        @Override
        public void start(Stage primaryStage) throws Exception{
            Parent root = FXMLLoader.load(getClass().getResource("/view/MainMenu.fxml"));
            Image ico = new Image(new FileInputStream("src/main/resources/images/icon.png"));
            primaryStage.getIcons().add(ico);
            primaryStage.setTitle("Файловый каталог");
            primaryStage.setScene(new Scene(root, 282, 196));
            primaryStage.setResizable(false);
            primaryStage.show();
        }
        public static void main(String[] args) { launch(args); }
    }
