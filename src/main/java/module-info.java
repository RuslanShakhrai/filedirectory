module FileDirectories {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires com.fasterxml.jackson.databind;
    requires javafx.base;
    requires com.fasterxml.jackson.core;
    opens FileDirectories.MainMenuController to javafx.fxml;
    opens FileDirectories.Controller to javafx.base,javafx.fxml;
    opens FileDirectories.model to com.fasterxml.jackson.databind, javafx.base;
    exports FileDirectories;

}